import 'babel-polyfill'
import $ from 'jquery';
import whatInput from 'what-input'
import carousels from './lib/carousels'
//
import './dialogtech'
import googleAnalytics from './google-analytics'
import liveHelpNow from './live-help-now'

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

// Carousel
carousels()

// Google Analytics
googleAnalytics()

// Live Help Now
liveHelpNow()
